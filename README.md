# coverageのサンプル

### 使用したライブラリ
- unittest
- coverage

### 目的
- 標準入出力を含むプログラムのテスト手法の確認
- coverageの出力する項目の確認

### coverage json を得る方法
```bash
make all
```

### ファイル説明
- main.py
テスト対象のプログラム(関数である必要がある)
- test.py
main.pyをテストするプログラム
- input.txt
main.pyに与える入力値が書かれている


### 参考
- [Python で標準入出力のテストを書く方法（unittest, pytest）](https://zenn.dev/ryo_kawamata/articles/41873296559da1)
- [Coverage.py](https://coverage.readthedocs.io/en/6.5.0/)