image_name = "coverage_example"

build:
	docker build -t ${image_name} .
run:
	docker run -v $$PWD/:/app --rm ${image_name}
remove:
	docker rmi ${image_name}

all:
	docker build -t ${image_name} . \
	&& docker run -v $$PWD/:/app --rm ${image_name} \
	&& docker rmi ${image_name}